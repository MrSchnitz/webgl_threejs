import {Raycaster, Vector2, Vector3} from "three";
import Camera from "./Camera";
import Application from "./Application";


export const MouseTypes = {
    LEFT: 1,
    MIDDLE: 2,
    RIGHT: 3
};

export const ArrowsTypes = {
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39,
    SPACE: 32,
    ENTER: 13,
    W: 87,
    S: 83,
    A: 65,
    D: 68
};

class Controller {

    private mouse: Vector2;
    private rayCast: Raycaster;

    private OnLeftClick: (value: any) => void;
    private OnRightClick: (value: any) => void;

    private OnIntersect?: (object: any) => void;
    private sceneChildren: any;

    private ArrowUp: () => void;
    private ArrowDown: () => void;
    private ArrowLeft: () => void;
    private ArrowRight: () => void;
    private Space: () => void;
    private Enter: () => void;
    private W: () => void;
    private S: () => void;
    private A: () => void;
    private D: () => void;

    private KeyGeneralEvent: () => void;


    constructor() {
        this.mouse = new Vector2();
        this.mouse.x = this.mouse.y = -1;

        this.rayCast = new Raycaster();

        this.OnLeftClick = (value: any) => {};
        this.OnRightClick = (value: any) => {};

        // this.OnIntersect = (object: any) => {};

        this.ArrowUp = () => {};
        this.ArrowDown = () => {};
        this.ArrowLeft = () => {};
        this.ArrowRight = () => {};
        this.Space = () => {};
        this.Enter = () => {};
        this.W = () => {};
        this.S = () => {};
        this.A = () => {};
        this.D = () => {};
        this.KeyGeneralEvent = () => {};

        //BIND
        this.onKeyDown = this.onKeyDown.bind(this);
        // this.ArrowUp = this.ArrowUp.bind(this);
        // this.ArrowDown = this.ArrowDown.bind(this);
        // this.ArrowLeft = this.ArrowLeft.bind(this);
        // this.ArrowRight = this.ArrowRight.bind(this);
        // this.Space = this.Space.bind(this);

        this.onMouseClick = this.onMouseClick.bind(this);

        document.addEventListener("keydown", this.onKeyDown, false);
        document.addEventListener("click", this.onMouseClick, false);
    }

    public setOnLeftClick(onLeftClick: (value: any) => void): any {this.OnLeftClick = onLeftClick;}
    public setOnRightClick(onRightClick: (value: any) => void): any {this.OnLeftClick = onRightClick;}

    public setOnIntersect(onIntersect: (object: any) => void): any {this.OnIntersect = onIntersect;}

    public setArrowUp(arrowUp: () => void): any {this.ArrowUp = arrowUp;}
    public setArrowDown(arrowDown: () => void): any {this.ArrowDown = arrowDown;}
    public setArrowLeft(arrowLeft: () => void): any {this.ArrowLeft = arrowLeft;}
    public setArrowRight(arrowRight: () => void): any {this.ArrowRight = arrowRight;}
    public setSpace(space: () => void): any {this.Space = space;}
    public setEnter(enter: () => void): any {this.Enter = enter;}
    public setW(w: () => void): any {this.W = w;}
    public setS(s: () => void): any {this.S = s;}
    public setA(a: () => void): any {this.A = a;}
    public setD(d: () => void): any {this.D = d;}
    public setKeyGeneralEvent(keyGeneralEvent: () => void): any {this.KeyGeneralEvent = keyGeneralEvent;}

    public setSceneChildren(sceneChildren: any) {this.sceneChildren = sceneChildren};

    private onKeyDown(e: any) {
        if(e.keyCode == ArrowsTypes.LEFT)
            this.ArrowLeft();
        else if(e.keyCode == ArrowsTypes.RIGHT)
            this.ArrowRight();
        else if(e.keyCode == ArrowsTypes.UP)
            this.ArrowUp();
        else if(e.keyCode == ArrowsTypes.DOWN)
            this.ArrowDown();
        else if(e.keyCode == ArrowsTypes.SPACE)
            this.Space();
        else if(e.keyCode == ArrowsTypes.ENTER)
            this.Enter();
        else if(e.keyCode == ArrowsTypes.W)
            this.W();
        else if(e.keyCode == ArrowsTypes.S)
            this.S();
        else if(e.keyCode == ArrowsTypes.A)
            this.A();
        else if(e.keyCode == ArrowsTypes.D)
            this.D();
        else
            return;

        this.KeyGeneralEvent();
        // cubes.forEach(cube => cube.position.x += ADD);
    }

    private onMouseClick(e: any) {
        this.mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
        this.mouse.y = - (e.clientY / window.innerHeight) * 2 + 1;
        // this.mouse.z = 1;
        this.rayCast.setFromCamera(this.mouse, Camera.get());

        switch (e.which){
            case MouseTypes.RIGHT: {
                this.OnLeftClick(this.rayCast.ray.at(100, new Vector3()));
                break;
            }
            case MouseTypes.LEFT: {
                const intersects = this.rayCast.intersectObjects(this.sceneChildren);
                if (this.OnIntersect && intersects.length != 0) {
                    this.OnIntersect(intersects[0].object);
                } else {
                    this.OnLeftClick(this.rayCast.ray.at(100, new Vector3()));
                }
                break;
            }
            default: {
                return;
            }
        }


        // const intersects = this.rayCast.intersectObjects(this.sceneChildren);
        //
        // if(intersects.length > 0){
        //     if (this.OnIntersect && intersects.length != 0) {
        //         this.OnIntersect(intersects[0].object);
        //     }
        // } else {
        //     this.rayCast.setFromCamera(this.mouse, Camera.get());
        //     this.OnLeftClick(this.rayCast.ray.at(100, new Vector3()));
        // }
    }
}

//Singleton
const Singleton = new Controller();

export default Singleton;
import {BoxGeometry, Geometry, Material, Mesh, MeshBasicMaterial, MeshPhongMaterial, SphereGeometry} from "three";

export interface ObjectInterface {
    get: () => Mesh;
    animate: () => void;
}

export class Object {
    protected geometry: Geometry;
    protected material: Material;

    constructor(geometry: Geometry, material: Material){
        this.geometry = geometry;
        this.material = material;
    }
}


export class Cube extends Object{
    private object: Mesh;

    constructor(geometry: BoxGeometry = new BoxGeometry( 1, 1, 1 ), material: MeshBasicMaterial = new MeshBasicMaterial( { color: 0x00ff00 } )){
        super(geometry, material);
        this.object = new Mesh( this.geometry, this.material );
    }

    public animate() {
        this.object.rotation.x += 0.01;
        this.object.rotation.y += 0.01;
    };

    public get(){
        return this.object;
    }
}

export class Sphere extends Object{
    private object: Mesh;

    constructor(geometry: SphereGeometry, material: MeshPhongMaterial){
        super(geometry, material);
        this.object = new Mesh( this.geometry, this.material );
    }

    public animate() {

    };

    public get(){
        return this.object;
    }
}
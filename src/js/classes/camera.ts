import {Camera as WebGLCamera, OrthographicCamera, PerspectiveCamera} from "three";

export enum CameraType{
    PERSPECTIVE, ORTHOGONAL
}

interface CameraInterface {

}

class Camera{

    private camera: WebGLCamera;

    public constructor(){
        // if(type === CameraType.PERSPECTIVE) {
            this.camera = new PerspectiveCamera();
        // } else {
        //     this.camera = new OrthographicCamera(20,30,20,20);
        // }
        // this.camera = new Camera();
    }

    public setPerspectiveCamera(fov: number, aspect: number = window.innerWidth / window.innerHeight, near?: number, far?: number) {
        this.camera = new PerspectiveCamera(fov, aspect, near, far);
    }

    public setOrhogonalCamera(left: number, right: number, top: number, bottom: number) {
        this.camera = new OrthographicCamera(left,right,top,bottom);
    }

    public get(): WebGLCamera {
        return this.camera;
    }
    
}

//Singleton
const Singleton = new Camera();

export default Singleton;
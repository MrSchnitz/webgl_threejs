
export const randomInRange = (from: any, to: any) => {
    let x = Math.random() * (to - from);
    return x + from;
};
import {Camera as WebGLCamera, PerspectiveCamera, Scene as WebGLScene, WebGLRenderer} from "three";
import Camera from "./Camera";
import * as THREE from "three";
// import {renderer} from "./renderer";
import SceneManager from "./SceneManager";
import {ObjectInterface} from "./object";
import Controller from "./controller";

class Application {

    private renderer: WebGLRenderer;
    private sceneManager: SceneManager;

    private cameraMove: boolean = true;
    private cameraDirection: boolean = true;

    public constructor() {
        Camera.setPerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
        this.sceneManager = new SceneManager();

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize( window.innerWidth, window.innerHeight );

        this.init();

        // BIND
        this.start = this.start.bind(this);
    }

    private init() {
        Controller.setSpace(() => {
           this.cameraMove = !this.cameraMove;
        });
        Controller.setEnter(() => {
            this.cameraDirection = !this.cameraDirection;
        });
    }

    public get(){
        return ({
            sceneManager: this.sceneManager,
            renderer: this.renderer
        })
    }

    public start(){
        requestAnimationFrame( this.start );

        if(this.cameraMove) {
            if(this.cameraDirection) {
                Camera.get().position.z -= 0.05;
            } else {
                Camera.get().position.z += 0.05;
            }
        }

        // this.sceneManager.get().objects.forEach((object: ObjectInterface) => {
        //     object.animate();
        // });

        this.renderer.render( this.sceneManager.get().scene, Camera.get() );
    }
}

export default Application;
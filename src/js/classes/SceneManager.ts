import {BoxGeometry, Color, DirectionalLight, Mesh, MeshBasicMaterial, Scene as WebGLScene} from "three";
import * as THREE from "three";
import {Cube, ObjectInterface, Sphere} from "./object";
import Controller from "./controller";
import {randomInRange} from "./utils";
import Camera from "./Camera";

export interface SceneInterface {
    scene: WebGLScene;
    cube: Mesh;
    light: DirectionalLight;
}

class SceneManager {

    private scene: WebGLScene;
    private objects: any[];
    private light: DirectionalLight;

    private HORIZONTAL: number = 0.2;
    private VERTICAL: number = 0.2;

    constructor() {
        this.scene = new WebGLScene();
        Camera.get().position.z = 100;
        Camera.get().position.y = 5;

        this.objects = [];
        this.light = new DirectionalLight();
        this.createController();
        this.createScene();
        // this.createLight = this.createLight.bind(this);
    }

    public getScene(): WebGLScene {
        return this.scene;
    }

    public addObjectToScene(object: any): any {
        this.scene.add(object);
    }

    private createController(): any {
        Controller.setArrowUp(() => {
           // this.VERTICAL = 0.2;
            Camera.get().position.y += 0.2;
        });
        Controller.setArrowDown(() => {
            Camera.get().position.y -= 0.2;
            // this.VERTICAL = -0.2;
        });
        Controller.setArrowLeft(() => {
            Camera.get().position.x -= 0.2;
            // this.HORIZONTAL = -0.2;
        });
        Controller.setArrowRight(() => {
            Camera.get().position.x += 0.2;
            // this.HORIZONTAL = 0.2;
        });
        Controller.setW(() => {
            Camera.get().position.z -= 0.2;
        });
        Controller.setS(() => {
            Camera.get().position.z += 0.2;
        });

        Controller.setOnLeftClick((value: any) => {
            this.createSphere(value);
        });


        Controller.setSceneChildren(this.scene.children);

        Controller.setOnIntersect((object: any) => {
            this.objects.forEach((obj: any, index: any) => {
                if(obj.object == object){
                    this.objects.splice(index,1);
                    this.scene.remove(obj.object);
                }
            });
        });
    }

    public createScene():any {
        this.scene.background = new Color(0xffffee);
        this.createLight();

        // const cube = new Cube();
        // this.objects.push(cube);
        //
        // this.addObjectToScene(cube.get());

        for(let i = 1; i <= 150; i++) {
            this.createCube();
        }
    }

    private createLight(): any {
        this.light = new DirectionalLight(0xffffff, 1);
        this.addObjectToScene(this.light);
    }

    private createCube() {
        let w = randomInRange(1, 3);
        let h = randomInRange(1, 3);
        let d = randomInRange(1, 3);
        let geometry = new THREE.BoxGeometry(w, h, d);
        let material = new THREE.MeshLambertMaterial({color: Math.random() * 0xffffff});
        let cube = new Cube(geometry, material);
        cube.get().position.x = randomInRange(-40, 40);
        cube.get().position.z = randomInRange(-40, 40);

        this.objects.push(cube);
        this.addObjectToScene(cube.get());
    };

    private createSphere = (pos: any) => {
        console.log(pos);
        let material = new THREE.MeshPhongMaterial({color: 0X4a57fa,
            shininess: 100, side: THREE.DoubleSide});
        let geometry = new THREE.SphereGeometry(5, 30, 30);
        let sphere = new Sphere(geometry,material);
        sphere.get().position.set(pos.x, pos.y, pos.z);

        this.objects.push(sphere);
        this.addObjectToScene(sphere.get());
    };

    public get(){
        return ({
            scene: this.scene,
            objects: this.objects
        })
    }
}

export default SceneManager;